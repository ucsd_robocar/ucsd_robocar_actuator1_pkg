import rospy
from geometry_msgs.msg import Twist 
from adafruit_servokit import ServoKit


NODE_NAME = 'adafruit_twist_node'
TOPIC_NAME = '/cmd_vel'

class AdafruitTwist(Node):
    def __init__(self):
        super().__init__(NODE_NAME)
        self.rpm_subscriber = self.create_subscription(Twist, TOPIC_NAME, self.callback, 10)
        self.kit = ServoKit(channels=16)

        # Getting ROS parameters set from calibration Node
        self.steering_polarity = rospy.get_param('steering_polarity', int(1))
        self.throttle_polarity = rospy.get_param('throttle_polarity', int(1))

        # Display Parameters
        rospy.loginfo(
            f'\nsteering_polarity: {self.steering_polarity}'
            f'\nthrottle_polarity: {self.throttle_polarity}')

    def callback(self, msg):
        self.steering_polarity = rospy.get_param('steering_polarity') # ability to update steering polarity in real-time
        self.throttle_polarity = rospy.get_param('throttle_polarity') # ability to update throttle polarity in real-time
        # Steering map from [-1,1] --> [max_left,max_right] # to do: implement into calibration
        data_min_limit = -1
        data_max_limit = 1
        adafruit_min_limit = -1 # These will be rosparams eventually... : max_left
        adafruit_max_limit = 1  # These will be rosparams eventually... : max_right
        steering_angle = float(-0.1 + ((msg.angular.z-data_min_limit)*(adafruit_max_limit - adafruit_min_limit))/(data_max_limit-data_min_limit))

        # Send values to adafruit board
        self.kit.servo[1].angle = float(self.steering_polarity * 90 * (1 + msg.angular.z))
        self.kit.continuous_servo[2].throttle = self.throttle_polarity * msg.linear.x


def main():
    vesc_twist = AdafruitTwist()
    rate = rospy.Rate(15)
    while not rospy.is_shutdown():
        rospy.spin()
        rate.sleep()


if __name__ == '__main__':
    main()
