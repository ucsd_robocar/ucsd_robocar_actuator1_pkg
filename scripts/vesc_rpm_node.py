#!/usr/bin/python3
import rospy
from std_msgs.msg import Float32
from vesc_client import VESC_

RPM_NODE_NAME = 'vesc_rpm_node'
RPM_REQUEST_TOPIC_NAME = '/throttle'

v = VESC_()


def callback(data):
    max_rpm = rospy.get_param('max_rpm')
    normalized_throttle = data.data
    rpm = int(max_rpm*normalized_throttle)
    v.send_rpm(rpm)


if __name__ == '__main__':
    rospy.init_node(RPM_NODE_NAME, anonymous=False)
    rospy.Subscriber(RPM_REQUEST_TOPIC_NAME, Float32, callback)
    rate = rospy.Rate(30)
    while not rospy.is_shutdown():
        rate.sleep()
        rospy.spin()
