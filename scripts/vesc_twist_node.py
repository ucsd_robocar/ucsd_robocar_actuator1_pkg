#!/usr/bin/python3
import rospy
from geometry_msgs.msg import Twist 
from vesc_client import VESC_

NODE_NAME = 'vesc_twist_node'
TOPIC_NAME = '/cmd_vel'


class VescTwist:
    def __init__(self):
        # Initialize node and create publishers/subscribers
        self.init_node = rospy.init_node(NODE_NAME, anonymous=False)
        self.twist_subscriber = rospy.Subscriber(TOPIC_NAME, Twist, self.callback)
        self.vesc = VESC_()

        # Getting ROS parameters set from calibration Node
        self.default_rpm = 5000
        self.max_rpm = rospy.get_param('max_rpm', self.default_rpm)
        self.steering_polarity = rospy.get_param('steering_polarity', 1)
        self.throttle_polarity = rospy.get_param('throttle_polarity', 1)

        # Set initial ROS parameters
        rospy.set_param('/max_rpm', self.max_rpm)
        rospy.set_param('/steering_polarity', self.steering_polarity)
        rospy.set_param('/throttle_polarity', self.throttle_polarity)

        # Display Parameters
        rospy.loginfo(
            f'\nmax_rpm: {self.max_rpm}'
            f'\nsteering_polarity: {self.steering_polarity}'
            f'\nthrottle_polarity: {self.throttle_polarity}')

    def callback(self, msg):
        # Dynamically update params
        self.max_rpm = rospy.get_param('/max_rpm')
        self.steering_polarity = rospy.get_param('/steering_polarity')
        self.throttle_polarity = rospy.get_param('/throttle_polarity')

        # Steering map from [-1,1] --> [0,1]  
        data_min_limit = -1
        data_max_limit = 1 
        vesc_min_limit = 0 # These will be rosparams eventually... : max_left
        vesc_max_limit = 1 # These will be rosparams eventually... : max_right
        steering_angle = float(-0.1 + ((msg.angular.z-data_min_limit)*(vesc_max_limit - vesc_min_limit))/(data_max_limit-data_min_limit))

        # RPM map from [-1,1] --> [-max_rpm,max_rpm]
        rpm = self.max_rpm * msg.linear.x

        # Send commands to Vesc
        self.vesc.send_rpm(int(self.throttle_polarity * rpm))
        self.vesc.send_servo_angle(float(self.steering_polarity * steering_angle))


def main():
    vesc_twist = VescTwist()
    rate = rospy.Rate(15)
    while not rospy.is_shutdown():
        rospy.spin()
        rate.sleep()


if __name__ == '__main__':
    main()
